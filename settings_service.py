from typing import Union, Optional

import yaml


class ConfigError(ValueError):
    pass


class Settings:
    def __init__(self):
        with open('settings.yaml') as f:
            self.data = yaml.safe_load(f)

    def get_sentry_dsn(self) -> Union[None, str]:
        if 'sentry_dsn' not in self.data:
            return None

        return self.data['sentry_dsn']

    def get_token(self) -> str:
        if 'token' not in self.data:
            raise ConfigError('A token needs to be defined in settings.yaml!')

        return self.data['token']

    def get_prefix(self) -> str:
        if 'prefix' not in self.data:
            return '-'

        return self.data['prefix']

    def do_delete_invoking(self) -> bool:
        if 'delete_invoking' not in self.data:
            return False

        return self.data['delete_invoking']

    def do_delete_reply(self) -> bool:
        if 'delete_reply' not in self.data:
            return False

        return self.data['delete_reply']

    def get_allowed_role_ids(self) -> [str]:
        if 'allowed_role_ids' not in self.data:
            return []

        return self.data['allowed_role_ids']

    def get_owner_id(self) -> Optional[str]:
        if 'owner_id' not in self.data:
            return None

        return self.data['owner_id']

    def get_spam_channels(self) -> [str]:
        if 'spam_channels' not in self.data:
            return []

        return self.data['spam_channels']

    def get_clean_channels(self) -> [str]:
        if 'clean_channels' not in self.data:
            return []

        return self.data['clean_channels']

    def get_clean_channels_timeout(self) -> int:
        if 'clean_channels_timeout' not in self.data:
            return 60

        return int(self.data['clean_channels_timeout'])

    def get_auto_responses(self) -> [{str: str}]:
        if 'auto_responses' not in self.data:
            return []

        return self.data['auto_responses']
