import logging

from bot import JosuBot


bot = JosuBot()
try:
    bot.startup()
except KeyboardInterrupt:
    try:
        logging.info('Got KeyboardInterrupt, kthxbai')
    except:
        pass
