from functools import wraps

from discord import Message

from bot import JosuBot
from custom_exceptions import StyledUserNotification


def check_author_has_allowed_role(bot: JosuBot, message: Message) -> bool:
    """
    Checks if the author of the given message has an allowed role as defined in the
     bot's settings.
    """
    if message.server is not None:
        author_roles = set([r.id for r in message.author.roles])
        allowed_roles = set(bot.settings.get_allowed_role_ids())
        if len(allowed_roles.intersection(author_roles)) == 0:
            return False

    return True

def ensure_author_has_allowed_role(cmd_func):
    """
    Wrapper for a cmd function which ensures, that the author of the message has an
     allowed role as defined in the bot's settings.
    If not, it aborts by raising a StyledUserNotification.
    """
    @wraps(cmd_func)
    async def wrapped(*args, **kwargs):
        bot = args[0]
        message = args[1]
        if not (
                isinstance(bot, JosuBot) and isinstance(message, Message)
        ):
            return await cmd_func

        if not (check_author_has_allowed_role(bot, message) or message.author.id == bot.settings.get_owner_id()):
            raise StyledUserNotification('ノ┬─┬ノ ︵ ( \o°o)\\  Du bist dafür nicht kompetent genug!')

        return await cmd_func(*args, **kwargs)

    return wrapped

def ensure_author_is_owner(cmd_func):
    """
    Wrapper for a cmd function which ensures, that the author of the message is the
     owner as defined in the bot's settings.
    If not, it aborts by raising a StyledUserNotification.
    """
    @wraps(cmd_func)
    async def wrapped(*args, **kwargs):
        bot = args[0]
        message = args[1]
        if not (
                isinstance(bot, JosuBot) and isinstance(message, Message)
        ):
            return await cmd_func

        if not message.author.id == bot.settings.get_owner_id():
            raise StyledUserNotification('ノ┬─┬ノ ︵ ( \o°o)\\  Du bist dafür nicht fähig genug!')

        return await cmd_func(*args, **kwargs)

    return wrapped
