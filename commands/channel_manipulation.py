from datetime import datetime

import discord
from discord import Message

from bot import JosuBot
from custom_exceptions import StyledUserNotification, TABLE_FLIPPING_EMOJI
from utils import ensure_author_has_allowed_role


@ensure_author_has_allowed_role
async def cmd_delete(bot: JosuBot, message: Message, command: str, args: [str]):
    """
    `{prefix}delete <Anzahl>`
    Löscht die angegebene Anzahl an Nachrichten.
    Die eigene Nachricht, die deisen Command aufruft, wird nicht mitgezählt.
    Nachrichten, die älter als zwei Wochen sind, können nicht gelöscht werden.
    """
    await bot.send_typing(message.channel)

    if len(args) != 1:
        raise StyledUserNotification(
            TABLE_FLIPPING_EMOJI + 'Du musst genau eine Zahl als Anzahl der zu löschenden Nachrichten angeben!'
        )

    try:
        search_range = int(args[0])
        if search_range <= 0:
            raise ValueError()
    except ValueError:
        raise StyledUserNotification(
            TABLE_FLIPPING_EMOJI + 'Der angegebene Wert `%s` ist keine gültige Zahl!' % args[0]
        )

    count = 0
    async for entry in bot.logs_from(message.channel, search_range, before=message):
        if (entry.timestamp - datetime.now()).days > -14:
            await bot.send_typing(message.channel)

            try:
                await bot.delete_message(entry)
                count += 1
            except discord.Forbidden:
                pass
            except discord.HTTPException:
                pass

    return await bot.send_message(
        message.channel, 'Erfolgreich %s Nachricht%s gelöscht.%s' % (
            count, 'en' * bool(count),
            '(In privaten Nachrichten kann jede Person nur seine eigenen Nachrichten löschen.)' * (message.server is None)
        )
    )
