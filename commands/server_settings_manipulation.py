from discord import Message, Color, Permissions, PermissionOverwrite

from bot import JosuBot
from custom_exceptions import StyledUserNotification, TABLE_FLIPPING_EMOJI
from utils import ensure_author_has_allowed_role


@ensure_author_has_allowed_role
async def cmd_createroles(bot: JosuBot, message: Message, command: str, args: [str]):
    """
    `{prefix}createroles <Rollen-Name(n)>`
    Erstellt mehrere Rollen mit Standard-Farbe und Standard-Berechtigungen und listet ihre IDs auf.
    Rollen-Namen müssen mit `, ` getrennt werden.
    """
    await bot.send_typing(message.channel)

    if message.server is None:
        raise StyledUserNotification(TABLE_FLIPPING_EMOJI + 'Dieser Command muss auf einem Server aufgerufen werden!')

    args = ' '.join(args).split(', ')

    if args[0] == '':
        return await bot.send_message(message.channel, TABLE_FLIPPING_EMOJI + 'Du musst Namen angeben.')

    role_ids = {}
    for name in args:
        await bot.send_typing(message.channel)

        r = await bot.create_role(
            server=message.server, name=name, permissions=Permissions.none(),
            color=Color.light_grey(), hoist=False, mentionable=False
        )
        role_ids.update({name : r.id})

    txt = 'Erstellte Rollen:'
    for name, rid in role_ids.items():
        txt += '\n`%s` : `%s`' % (name, rid)
    return await bot.send_message(message.channel, txt)


@ensure_author_has_allowed_role
async def cmd_addchanneloverwrites(bot: JosuBot, message: Message, command: str, args: [str]):
    """
    `{prefix}addchanneloverwrites <Rollen-ID> <allow Permission-Integer> <deny Permission-Integer>`
    Fügt einen Permission-Overwrite für die gegebene Rolle für alle Text- und Voice-Channels hinzu.
    Die Werte, die weder in allow noch in deny sind, werden nicht geändert ("grauer Zustand").
    """
    if message.server is None:
        raise StyledUserNotification(TABLE_FLIPPING_EMOJI + 'Dieser Command muss auf einem Server aufgerufen werden!')

    await bot.send_typing(message.channel)

    role = None
    for r in message.server.roles:
        if r.id == args[0]:
            role = r
            break

    if role is None:
        raise RuntimeError('Die angegebene Rollen-ID ist ungültig!')

    try:
        allow_int = int(args[1])
    except ValueError:
        raise RuntimeError('Der angegebene allow Integer ist keine gültige Zahl!')

    try:
        deny_int = int(args[2])
    except ValueError:
        raise RuntimeError('Der angegebene deny Integer ist keine gültige Zahl!')

    allow_perm = Permissions(allow_int)
    deny_perm  = Permissions(deny_int)
    overwrite  = PermissionOverwrite.from_pair(allow_perm, deny_perm)

    for channel in message.server.channels:
        await bot.send_typing(message.channel)

        await bot.edit_channel_permissions(channel, role, overwrite)

    return await bot.send_message(message.channel, ':ok_hand:')
