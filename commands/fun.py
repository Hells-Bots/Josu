import random

from discord import Message

from bot import JosuBot
from custom_exceptions import StyledUserNotification, TABLE_FLIPPING_EMOJI


async def cmd_pick(bot: JosuBot, message: Message, command: str, args: [str]):
    """
    `{prefix}pick <optionen>`
    Wählt zufällig eine der Optionen aus. Optionen mit `,` trennen
    """

    options = ' '.join(args).split(',')
    if len(options) < 2:
        raise StyledUserNotification(TABLE_FLIPPING_EMOJI + 'Du musst mindestens zwei Optionen angeben!')

    choice = random.choice(options).strip()
    thing = random.choice(['Glaskugel', 'magische Kartoffel', 'weibliche Intuition'])

    await bot.send_message(message.channel, 'Meine %s sagt %s.' % (thing, choice))
