from discord import Message, Embed, ChannelType, Server, Role
from io import BytesIO

from bot import JosuBot
from custom_exceptions import StyledUserNotification, TABLE_FLIPPING_EMOJI
from utils import ensure_author_has_allowed_role


def _format_time(t):
    return '%02d.%02d.%s %02d:%02d' % (
        t.day, t.month, t.year, t.hour, t.minute
    )

def _format_bool(b):
    return 'ja' if b else 'nein'


@ensure_author_has_allowed_role
async def cmd_userinfo(bot: JosuBot, message: Message, command: str, args: [str]):
    """
    `{prefix}userinfo <User-Mention>` oder `{prefix}userinfo <User-ID>`
    Zeigt wichtige Infos über den angegebenen User an.
    """
    await bot.send_typing(message.channel)

    if message.server is not None:
        users = {m : True for m in message.mentions}
        message_server_members = {m.id : m for m in message.server.members}
    else:
        users = {m : False for m in message.mentions}
        message_server_members = {}

    for uid in args:
        if uid in message_server_members:
            users.update({message_server_members[uid] : True})
        else:
            try:
                u = await bot.get_user_info(uid)
                users.update({u : False})
            except BaseException:
                pass

    if len(users) == 0:
        raise StyledUserNotification(TABLE_FLIPPING_EMOJI + 'Du musst einen User angeben!')

    for user, is_member in users.items():
        embed = Embed(
            title='Info über %s' % user.display_name,
            description='Globaler Name: %s\nSnowflake ID: %s\nAvatar-Bild: [Link](%s)' % (
                user.name, user.id, user.avatar_url.replace('webp', 'png')
            ),
            color=0x565d66,
            inline=False
        )\
            .set_thumbnail(url=user.avatar_url)\
            .add_field(name='Discord beigetreten am:', value=_format_time(user.created_at))

        if is_member:
            embed\
                .add_field(name='Server beigetreten am:', value=_format_time(user.joined_at))\
                .add_field(
                    name='Rollen:',
                    value='`' + '`, `'.join([r.name for r in user.roles[1:]]) + '`',
                    inline=False
                )\
                .add_field(
                    name='serverweite Berechtigungen:',
                    value='`' + '`, `'.join([str(p[0]) for p in user.server_permissions if p[1]]) + '`',
                    inline=False
                )

        await bot.send_message(message.channel, embed=embed)


def _resolve_role_name_to_role(bot: JosuBot, server: Server, name: str) -> Role:
    name = name.lower()

    for role in server.roles:
        if name in role.name.lower():
            return role

    raise ValueError('Der angegebene Name passt zu keiner Rolle auf dem gegebenen Server!')

@ensure_author_has_allowed_role
async def cmd_roleid(bot: JosuBot, message: Message, command: str, args: [str]):
    """
    `{prefix}roleid <Rollen-Name>`
    Zeigt die ID der angegebenen Rolle an.
    """
    try:
        role = _resolve_role_name_to_role(bot, message.server, args[0])
    except ValueError:
        raise StyledUserNotification(TABLE_FLIPPING_EMOJI + 'Der angegebene Name wurde nicht gefunden!')

    await bot.send_message(message.channel, '`%s` : `%s`' % (role.name, role.id))

@ensure_author_has_allowed_role
async def cmd_roleinfo(bot: JosuBot, message: Message, command: str, args: [str]):
    """
    `{prefix}roleinfo <Rollen-ID>` oder `{prefix}roleinfo <Rollen-Name>`
    Zeigt wichtige Infos über die angegebene Rolle an.
    """
    if message.server is None:
        raise StyledUserNotification(TABLE_FLIPPING_EMOJI + 'Dieser Command muss auf einem Server aufgerufen werden!')

    await bot.send_typing(message.channel)

    role = None
    for r in message.server.roles:
        if r.id == args[0]:
            role = r
            break
    if role is None:
        try:
            role = _resolve_role_name_to_role(bot, message.server, args[0])
        except ValueError:
            raise StyledUserNotification(TABLE_FLIPPING_EMOJI + 'Du musst eine gültige Rolle angeben!')

    embed = Embed(
        title='Info über %s' % role.name,
        description='Snowflake ID: ' + role.id,
        color=role.color.value,
        inline=False
    )\
        .add_field(
            name='erstellt am:',
            value=_format_time(role.created_at),
            inline=False
        )\
        .add_field(name='Rang (@everyone hat 0)', value=str(role.position))\
        .add_field(name='@fähig?', value=_format_bool(role.mentionable))\
        .add_field(name='separat angezeigt?', value=_format_bool(role.hoist))\
        .add_field(name='extern verwaltet?', value=_format_bool(role.managed))\
        .add_field(
            name='Farbe:',
            value='`#{r:02x}{g:02x}{b:02x}`  `rgb({r}, {g}, {b})`'.format(
                r=role.color.r, g=role.color.g, b=role.color.b
            ),
            inline=False
        )\
        .add_field(
            name='serverweite Berechtigungen:',
            value='`' + '`, `'.join([str(p[0]) for p in role.permissions if p[1]]) + '`',
            inline=False
        )

    await bot.send_message(message.channel, embed=embed)


@ensure_author_has_allowed_role
async def cmd_listids(bot: JosuBot, message: Message, command: str, args: [str]):
    """
    `{prefix}listids`
    Listet IDs für die Channel, Rollen und User auf diesem Server auf.
    """
    if message.server is None:
        raise StyledUserNotification(TABLE_FLIPPING_EMOJI + 'Dieser Command muss auf einem Server aufgerufen werden!')

    await bot.send_typing(message.channel)

    data = ['Your ID: %s' % message.author.id]

    data.append("\nUser IDs:")
    data.extend(['%s #%s: %s' % (m.name, m.discriminator, m.id) for m in message.server.members])

    data.append("\nRole IDs:")
    data.extend(['%s: %s' % (r.name, r.id) for r in message.server.roles])

    data.append("\nText Channel IDs:")
    tcs = [c for c in message.server.channels if c.type == ChannelType.text]
    data.extend(['%s: %s' % (c.name, c.id) for c in tcs])

    data.append("\nVoice Channel IDs:")
    vcs = [c for c in message.server.channels if c.type == ChannelType.voice]
    data.extend('%s: %s' % (c.name, c.id) for c in vcs)

    with BytesIO() as sdata:
        sdata.writelines(d.encode('utf8') + b'\n' for d in data)
        sdata.seek(0)

        await bot.send_file(message.author, sdata, filename='%s-ids.txt' % (message.server.name.replace(' ', '_')))

    return await bot.send_message(message.channel, '\N{OPEN MAILBOX WITH RAISED FLAG}')
