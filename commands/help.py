from textwrap import dedent

from discord import Message

from bot import JosuBot
from utils import check_author_has_allowed_role


async def cmd_help(bot: JosuBot, message: Message, command: str, args: [str]):
    """
    `{prefix}help`
    Zeigt eine Liste an verfügbaren Befehlen an.
    """
    if bot.user.id not in message.raw_mentions:
        return

    if not check_author_has_allowed_role(bot, message):
        return

    help_list = []

    import commands
    for attribute in dir(commands):
        if attribute.startswith('cmd_'):
            help_text = dedent(getattr(commands, attribute).__doc__.format(prefix=bot.settings.get_prefix()))
            help_text_lines = help_text.split('\n')[1:-1]

            help_list.append(':small_orange_diamond: %s' % (
                '\n         '.join(help_text_lines)
            ))

    return await bot.send_message(message.channel, '\n' + '\n'.join(help_list))
