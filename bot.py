import asyncio
import logging
import traceback
import os

from raven import fetch_git_sha
from raven.handlers.logging import Client as RavenClient, SentryHandler

from discord import Game, Client as DiscordClient, errors, Message, NotFound

from custom_exceptions import StyledUserNotification
from settings_service import Settings


class JosuBot(DiscordClient):
    def __init__(self, **options):
        super().__init__(**options)

        self.settings = Settings()

        root_logger = logging.getLogger()
        root_logger.setLevel(logging.INFO)

        f_handler = logging.FileHandler('run.log', 'w', encoding='UTF-8')
        formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(name)s | %(message)s')
        f_handler.setFormatter(formatter)
        root_logger.addHandler(f_handler)

        if self.settings.get_sentry_dsn() is not None:
            self.r_client = RavenClient(
                self.settings.get_sentry_dsn(),
                auto_log_stacks=True,
                release=fetch_git_sha(os.path.dirname(__file__))
            )
        else:
            self.r_client = None

    def startup(self):
        token = self.settings.get_token()

        try:
            self.loop.run_until_complete(self.start(token))
        except errors.LoginFailure:
            raise RuntimeError('Please fix the given Token-Value inside settings.yaml!')

    async def on_ready(self):
        await self.change_presence(
            game=Game(name="{}help".format(self.settings.get_prefix()))
        )

    async def on_message(self, message: Message):
        await self.wait_until_ready()

        if self.r_client:
            self.r_client.context.activate()
            tags_dict = {
                'message_content': message.content,
                'discord_author_display_name': message.author.display_name,
                'discord_author_id': message.author.id
            }
            if message.server is not None and message.channel is not None:
                tags_dict.update({
                    'discord_server_name': message.server.name,
                    'discord_server_id': message.server.id,
                    'discord_channel_name': message.channel.name,
                    'discord_channel_id': message.channel.id,
                })
            else:
                tags_dict.update({
                    'discord_server_name': '[private]',
                    'discord_server_id': 'n/a',
                    'discord_channel_name': 'n/a',
                    'discord_channel_id': 'n/a',
                })
            self.r_client.context.merge({'tags': tags_dict})

        try:
            # Preflight - Checks:
            if message.author == self.user:
                return

            content = message.content.strip()
            if not content.startswith(self.settings.get_prefix()):
                await self._handle_message_filters(message, content)
            else:
                await self._handle_cmd(message, content)

            await self._clean(message)

        finally:
            if self.r_client:
                self.r_client.context.clear()

    async def _handle_message_filters(self, message: Message, content: str):
        # no command, run filters:
        if message.channel.id in self.settings.get_spam_channels():
            return

        any_filter_fired = False

        import message_filters
        for attribute in dir(message_filters):
            if attribute.startswith('mf_'):
                active_filter = getattr(message_filters, attribute)
                try:
                    this_filter_fired = await active_filter(self, message, content)
                    any_filter_fired = this_filter_fired or any_filter_fired
                    if this_filter_fired:
                        logging.info(
                            'Message Filter "%s" got triggered - s: "%s" - c: "%s" - u: "%s %s" - m: "%s"' % (
                                attribute,
                                message.server.name if message.server is not None else '[private]',
                                message.channel.name if message.server is not None else 'n/a',
                                message.author.display_name,
                                message.author.mention,
                                content
                            )
                        )
                except StyledUserNotification as sun:
                    logging.info('cmd done, with User Notification "' + str(sun) + '"')
                except BaseException as ex:
                    if self.r_client:
                        self.r_client.captureException(exec_info=True)
                    logging.error(str(ex))
                    logging.error(traceback.format_exc())

    async def _handle_cmd(self, message: Message, content: str):
        # Unpacking:
        command, *args = content.split(self.settings.get_prefix(), 1)[-1].split(' ')

        if len(command) == 0:
            return

        if self.r_client:
            self.r_client.context.merge({'tags': {
                'command': command
            }})

        # Get Handler:
        try:
            import commands
            handler = getattr(commands, 'cmd_' + command, None)
        except BaseException as ex:
            logging.error('Error while trying to load the command handlers: ' + str(ex))
            return

        if not handler:
            return

        reply = None

        # Okay, let's do this!
        logging.info('got cmd: s: "%s" - c: "%s" - u: "%s %s" - m: "%s"' % (
            message.server.name if message.server is not None else '[private]',
            message.channel.name if message.server is not None else 'n/a',
            message.author.display_name,
            message.author.mention,
            content
        ))
        try:
            reply = await handler(self, message, command, args)
            logging.info('cmd done')

        except StyledUserNotification as sun:
            logging.info('cmd done, with User Notification "' + str(sun) + '"')

            reply = await self.send_message(
                message.channel, str(sun)
            )
            if self.settings.do_delete_invoking() and message.server is not None:
                await asyncio.sleep(17)
                await self.delete_message(reply)

        except BaseException as ex:
            if self.r_client:
                self.r_client.captureException(exec_info=True)
            logging.error(str(ex))
            logging.error(traceback.format_exc())

            reply = await self.send_message(
                message.channel, '¯\\_(ツ)_/¯  ' + str(ex)
            )
            if self.settings.do_delete_invoking() and message.server is not None:
                await asyncio.sleep(17)
                await self.delete_message(reply)

        finally:
            if self.settings.do_delete_invoking() and message.server is not None:
                await self.delete_message(message)

            if self.settings.do_delete_reply() and reply is not None:
                await asyncio.sleep(30)
                await self.delete_message(reply)

    async def _clean(self, message: Message):
        if message.channel.id in self.settings.get_clean_channels():
            await asyncio.sleep(self.settings.get_clean_channels_timeout())
            try:
                await self.delete_message(message)
                logging.info(
                    'Message got cleaned - s: "%s" - c: "%s" - u: "%s %s" - m: "%s"' % (
                        message.server.name if message.server is not None else '[private]',
                        message.channel.name if message.server is not None else 'n/a',
                        message.author.display_name,
                        message.author.mention,
                        message.content
                    )
                )
            except NotFound:
                pass
