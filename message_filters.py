import logging
import re
from typing import Union

from discord import Message

from bot import JosuBot


async def mf_keine_caps(bot: JosuBot, message: Message, content: str) -> bool:
    letter_content = re.sub(r'[^a-zA-ZöäüÖÄÜ]', '', content)
    if len(letter_content) < 3:
        return False

    if letter_content.upper() == letter_content:
        await bot.send_message(message.channel, message.author.mention + ', NEIN! Keine Caps!')
        return True

    return False


async def mf_auto_response_from_settings(bot: JosuBot, message: Message, content: str) -> Union[bool, str]:
    content = content.lower()

    triggered = []

    for auto_response in bot.settings.get_auto_responses():
        if 'trigger' not in auto_response or 'response' not in auto_response:
            logging.warning('Invalid auto response configured: ' + str(auto_response))
            continue

        if auto_response['trigger'].lower() in content:
            await bot.send_message(message.channel, message.author.mention + ', ' + auto_response['response'])
            triggered.append(auto_response['trigger'])

    if len(triggered) == 0:
        return False
    else:
        return 'found triggers: "' + '", "'.join(triggered) + '"'
